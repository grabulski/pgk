#include <cmath>
#include "color.h"

Color::Color()
{

}

Color::Color(int _r_h, int _g_s, int _b_v)
    : r_h(_r_h)
    , g_s(_g_s)
    , b_v(_b_v)
{

}

Color Color::toRGB()
{
    struct rgbf {
        float r;
        float g;
        float b;
    } rgbf = {0,0,0};

    int h = r_h;
    float s = float(g_s)/100.0f;
    float v = float(b_v)/100.0f;

//    float chroma = (1.0f - std::abs(2*v - 1.0f)) * s;
    float chroma = v * s;
    float hp = float(h)/60.0f;
    float x = chroma * (1.0f - std::abs(std::fmod(hp, 2.0f) - 1.0f));

    if (hp >= 0.f && hp < 1.f)
        rgbf = {chroma, x, 0.f};
    else if (hp >= 1.f && hp < 2.f)
        rgbf = {x, chroma, 0.f};
    else if (hp >= 2.f && hp < 3.f)
        rgbf = {0.f, chroma, x};
    else if (hp >= 3.f && hp < 4.f)
        rgbf = {0.f, x, chroma};
    else if (hp >= 4.f && hp < 5.f)
        rgbf = {x, 0.f, chroma};
    else if (hp >= 5.f && hp < 6.f)
        rgbf = {chroma, 0.f, x};

//    float m = v - chroma/2.f;
    float m = v - chroma;

    return Color((rgbf.r + m)*255, (rgbf.g + m)*255, (rgbf.b + m)*255);
}
