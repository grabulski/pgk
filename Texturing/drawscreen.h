#ifndef DRAWSCREEN_H
#define DRAWSCREEN_H

#include "imagecontainer.h"

#include <QHBoxLayout>
#include <QWidget>

class DrawScreen : public QWidget
{
    Q_OBJECT

private:
    QHBoxLayout layout;
    ImageContainer source;
    ImageContainer target;

public:
    explicit DrawScreen(QWidget *parent = nullptr);

    void resetAndResize(QString newImageName);

public slots:
    void reset();
    void open();

};

#endif // DRAWSCREEN_H
