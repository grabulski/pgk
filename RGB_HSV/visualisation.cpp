#include "visualisation.h"
#include "color.h"

Visualisation::Visualisation(Axis _axis)
    : axis(_axis)
    , image(256, 256, QImage::Format_RGB32)
{
    switch (axis) {
        case Axis::Red:
        case Axis::Green:
        case Axis::Blue:
            visType = VisType::RGB;
            drawRGB();
            break;
        case Axis::Hue:
        case Axis::Saturation:
        case Axis::Value:
            visType = VisType::HSV;
            drawHSV(0,0,0);
            break;
        default: break;
    }
}

void Visualisation::redraw(int r_h, int g_s, int b_v)
{
    if (visType == VisType::RGB)
        redrawRGB(r_h, g_s, b_v);
    else if (visType == VisType::HSV)
        drawHSV(r_h, g_s, b_v);
}

void Visualisation::redrawRGB(int r, int g, int b)
{
    for (int i = 0; i < image.height(); ++i) {
        uchar *line = image.scanLine(i);
        for (int j = 0; j < image.width(); ++j) {
            switch (axis) {
                case Axis::Red: line[j*4+2] = r; break;
                case Axis::Green: line[j*4+1] = g; break;
                case Axis::Blue: line[j*4] = b; break;
                default: break;
            }
        }
    }
}

void Visualisation::drawRGB()
{
    Color u_l, u_r, l_l, l_r;
    if (axis == Axis::Red) {
        u_l = Color(0, 255, 0);
        u_r = Color(0, 255, 255);
        l_l = Color(0, 0, 0);
        l_r = Color(0, 0, 255);
    } else if (axis == Axis::Green) {
        u_l = Color(255, 0, 0);
        u_r = Color(0, 0, 0);
        l_l = Color(255, 0, 255);
        l_r = Color(0, 0, 255);
    } else if (axis == Axis::Blue) {
        u_l = Color(255, 255, 0);
        u_r = Color(0, 255, 0);
        l_l = Color(255, 0, 0);
        l_r = Color(0, 0, 0);
    }

    for (int i = 0; i < image.height(); ++i) {
        uchar *line = image.scanLine(i);
        float currentHeight = float(i)/image.height();

        for (int j = 0; j < image.width(); ++j) {
            float currentWidth = float(j)/image.width();

            Color upper(
                        u_l.r_h*(1.0f-currentWidth) + u_r.r_h*currentWidth,
                        u_l.g_s*(1.0f-currentWidth) + u_r.g_s*currentWidth,
                        u_l.b_v*(1.0f-currentWidth) + u_r.b_v*currentWidth
                        );

            Color lower(
                        l_l.r_h*(1.0f-currentWidth) + l_r.r_h*currentWidth,
                        l_l.g_s*(1.0f-currentWidth) + l_r.g_s*currentWidth,
                        l_l.b_v*(1.0f-currentWidth) + l_r.b_v*currentWidth
                        );

            Color avg(
                      upper.r_h*(1.0f-currentHeight) + lower.r_h*currentHeight,
                      upper.g_s*(1.0f-currentHeight) + lower.g_s*currentHeight,
                      upper.b_v*(1.0f-currentHeight) + lower.b_v*currentHeight
                        );

            line[j*4]   = avg.b_v; // Blue
            line[j*4+1] = avg.g_s; // Green
            line[j*4+2] = avg.r_h; // Red
            line[j*4+3] = 255; // Alpha
        }
    }
}

void Visualisation::drawHSV(int _h, int _s, int _v)
{
    int h = _h, s = _s, v = _v;

    for (int y = 0; y < 256; ++y) {
        uchar *line = image.scanLine(y);

        if (axis == Axis::Hue || axis == Axis::Saturation)
            v = (y*100)/255;
        else if (axis == Axis::Value)
            s = (y*100)/255;

        for (int x = 0; x < 256; ++x) {

            if (axis == Axis::Hue)
                s = (x*100)/256;
            else if (axis == Axis::Saturation || axis == Axis::Value)
                h = (x*360)/256;

            Color c_hsv(h, s, v);
            Color c_rgb = c_hsv.toRGB();

            line[x*4]   = c_rgb.b_v; // Blue
            line[x*4+1] = c_rgb.g_s; // Green
            line[x*4+2] = c_rgb.r_h; // Red
            line[x*4+3] = 255; // Alpha
        }
    }
}
