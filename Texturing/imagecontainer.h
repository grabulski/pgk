#ifndef IMAGECONTAINER_H
#define IMAGECONTAINER_H

#include "fpoint.h"
#include "triangle.h"

#include <QWidget>

class ImageContainer : public QWidget
{
    Q_OBJECT
public:
    enum ContainerType {
        Target,
        Source
    } containerType = ContainerType::Source;

    QImage image;
    QImage overlay;
    ImageContainer *other;
    Triangle tri;
    bool locked = false;

    explicit ImageContainer(QWidget *parent = nullptr);

    void insertPixel(int x, int y, int r, int g, int b, int a, QImage &img);
    QColor getPixel(int x, int y);
    void drawLine(int x1, int y1, int x2, int y2, int r, int g, int b);
    void clearImage(QImage &img, int r, int g, int b, int a);
    void drawTriangle();

    void drawTexture();

private:
    std::vector<FPoint> getUVs();

public slots:
    void reset(bool);

protected:
    void paintEvent(QPaintEvent *);
    void mouseReleaseEvent(QMouseEvent *event);

};

#endif // IMAGECONTAINER_H
