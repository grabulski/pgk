#ifndef COLOR_H
#define COLOR_H


class Color
{
public:
    int r;
    int g;
    int b;
    Color();
    Color(int _r, int _g, int _b);
};

#endif // COLOR_H
