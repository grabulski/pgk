#include "drawscreen.h"
#include "color.h"

#include <QPainter>
#include <QRadioButton>

DrawScreen::DrawScreen(QWidget *parent)
    : QWidget(parent)
    , image(imageWidth, imageHeight, QImage::Format_RGB32)
{
    resize(imageWidth, imageHeight);
    clearScreen(0,0,0);

    visualisations["red"]           = Visualisation(Visualisation::Axis::Red);
    visualisations["green"]         = Visualisation(Visualisation::Axis::Green);
    visualisations["blue"]          = Visualisation(Visualisation::Axis::Blue);
    visualisations["hue"]           = Visualisation(Visualisation::Axis::Hue);
    visualisations["saturation"]    = Visualisation(Visualisation::Axis::Saturation);
    visualisations["value"]         = Visualisation(Visualisation::Axis::Value);
}

void DrawScreen::paintEvent(QPaintEvent *)
{
    QPainter p(this);

    p.fillRect(0, 0, width(), height(), Qt::black);

    if (currentVis != nullptr) {
        if (currentVis->visType == Visualisation::VisType::RGB)
            currentVis->redraw(red, green, blue);
        else if (currentVis->visType == Visualisation::VisType::HSV)
            currentVis->redraw(hue, saturation, value);
        p.drawImage(0, 0, currentVis->image);
    }
    else
        p.drawImage(0, 0, image);
}

void DrawScreen::setCurrentVisualisation()
{
    QObject *sender = QObject::sender();
    QString senderName = sender->objectName().toLower();
    const int prefixLen = QString("radio").length();
    senderName = senderName.mid(prefixLen);

    QMap<QString, Visualisation>::iterator it = visualisations.find(senderName);
    if (it != visualisations.end()) {
        currentVis = &it.value();
    } else {
        currentVis = nullptr;
    }

    update();
}

void DrawScreen::insertPixel(int x, int y, int r, int g, int b)
{
    if (x < 0 || y < 0 || x >= image.width() || y >= image.height())
        return;

    uchar *line = image.scanLine(y);
    line[x*4]   = b; // Blue
    line[x*4+1] = g; // Green
    line[x*4+2] = r; // Red
    line[x*4+3] = 255; // Alpha
}

void DrawScreen::clearScreen(int r, int g, int b)
{
    for (int y = 0; y < image.height(); ++y) {
        uchar *line = image.scanLine(y);
        for (int x = 0; x < image.width(); ++x) {
            line[x*4] = b; // Blue
            line[x*4+1] = g; // Green
            line[x*4+2] = r; // Red
            line[x*4+3] = 255; // Alpha
        }
    }
}
