#ifndef DRAWSCREEN_H
#define DRAWSCREEN_H

#include <QWidget>
#include "blendmodes.h"
#include "imagewithblending.h"

class DrawScreen : public QWidget
{
    Q_OBJECT

private:
    typedef Color (*blendFunc)(Color, Color);

    struct ImageToBlend {
        QImage image;
        float alpha;
        QString funcName;
    } imagesToBlend[3];

    const int imageWidth = 1200;
    const int imageHeight = 800;

    BlendModes blendModes;

    QImage background;
    QImage foreground;
    QImage foreground2;
    QImage foreground3;
    QImage blended;

    void paintEvent(QPaintEvent *);

public:
    explicit DrawScreen(QWidget *parent = nullptr);

private:
    void blendImages(QImage &foreground, QImage &background, QImage &blended, float alpha, const QString &funcName);
    void blendAllImages();

public slots:
    void blendModeChanged(const QString &mode);
    void alphaChanged(int a);
};

#endif // DRAWSCREEN_H
