#ifndef VISUALISATION_H
#define VISUALISATION_H

#include <QImage>
#include <QString>

class Visualisation
{
public:

    enum VisType {
        RGB,
        HSV
    } visType;

    enum Axis {
        Red,
        Green,
        Blue,
        Hue,
        Saturation,
        Value
    } axis;

    QImage image;
    int variableIndex = 1;

    Visualisation(Axis _axis);

    Visualisation() { }

    void redraw(int r_h, int g_s, int b_v);

private:
    void redrawRGB(int r, int g, int b);

    void drawRGB();
    void drawHSV(int _h, int _s, int _v);


};

#endif // VISUALISATION_H
