#include "blendmodes.h"

BlendModes::BlendModes()
{
    blendFunctions["normal"] = BlendModes::normal;
    blendFunctions["multiply"] = BlendModes::multiply;
    blendFunctions["screen"] = BlendModes::screen;
    blendFunctions["additive"] = BlendModes::additive;
    blendFunctions["difference"] = BlendModes::difference;
}
