/****************************************************************************
** Meta object code from reading C++ file 'renderer.h'
**
** Created by: The Qt Meta Object Compiler version 68 (Qt 6.0.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../renderer.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'renderer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 68
#error "This file was generated using the moc from 6.0.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Renderer_t {
    const uint offsetsAndSize[30];
    char stringdata0[76];
};
#define QT_MOC_LITERAL(ofs, len) \
    uint(offsetof(qt_meta_stringdata_Renderer_t, stringdata0) + ofs), len 
static const qt_meta_stringdata_Renderer_t qt_meta_stringdata_Renderer = {
    {
QT_MOC_LITERAL(0, 8), // "Renderer"
QT_MOC_LITERAL(9, 5), // "moveX"
QT_MOC_LITERAL(15, 0), // ""
QT_MOC_LITERAL(16, 1), // "x"
QT_MOC_LITERAL(18, 5), // "moveY"
QT_MOC_LITERAL(24, 1), // "y"
QT_MOC_LITERAL(26, 5), // "moveZ"
QT_MOC_LITERAL(32, 1), // "z"
QT_MOC_LITERAL(34, 4), // "rotX"
QT_MOC_LITERAL(39, 5), // "alpha"
QT_MOC_LITERAL(45, 4), // "rotY"
QT_MOC_LITERAL(50, 4), // "rotZ"
QT_MOC_LITERAL(55, 6), // "scaleX"
QT_MOC_LITERAL(62, 6), // "scaleY"
QT_MOC_LITERAL(69, 6) // "scaleZ"

    },
    "Renderer\0moveX\0\0x\0moveY\0y\0moveZ\0z\0"
    "rotX\0alpha\0rotY\0rotZ\0scaleX\0scaleY\0"
    "scaleZ"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Renderer[] = {

 // content:
       9,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags, initial metatype offsets
       1,    1,   68,    2, 0x0a,    0 /* Public */,
       4,    1,   71,    2, 0x0a,    2 /* Public */,
       6,    1,   74,    2, 0x0a,    4 /* Public */,
       8,    1,   77,    2, 0x0a,    6 /* Public */,
      10,    1,   80,    2, 0x0a,    8 /* Public */,
      11,    1,   83,    2, 0x0a,   10 /* Public */,
      12,    1,   86,    2, 0x0a,   12 /* Public */,
      13,    1,   89,    2, 0x0a,   14 /* Public */,
      14,    1,   92,    2, 0x0a,   16 /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    7,

       0        // eod
};

void Renderer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Renderer *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->moveX((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->moveY((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->moveZ((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->rotX((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->rotY((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->rotZ((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->scaleX((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->scaleY((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->scaleZ((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject Renderer::staticMetaObject = { {
    QMetaObject::SuperData::link<QWidget::staticMetaObject>(),
    qt_meta_stringdata_Renderer.offsetsAndSize,
    qt_meta_data_Renderer,
    qt_static_metacall,
    nullptr,
qt_incomplete_metaTypeArray<qt_meta_stringdata_Renderer_t

, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>


>,
    nullptr
} };


const QMetaObject *Renderer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Renderer::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Renderer.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int Renderer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType();
        _id -= 9;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
