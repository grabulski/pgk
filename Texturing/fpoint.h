#ifndef FPOINT_H
#define FPOINT_H

class FPoint
{
public:
    float x;
    float y;

    FPoint();
    FPoint(float _x, float _y);
};

#endif // FPOINT_H
