#ifndef IMAGEWITHBLENDING_H
#define IMAGEWITHBLENDING_H

#include <QImage>
#include "blendmodes.h"

class ImageWithBlending
{
public:
    QImage image;
    QImage *background;

    typedef Color (*blendFunc)(Color, Color) ;

    blendFunc currentFunc;

    ImageWithBlending(const QString &fileName, QImage *_background, blendFunc _func);
};

#endif // IMAGEWITHBLENDING_H
