#include "renderer.h"

#include <QPainter>
#include <utility>

Renderer::Renderer(QWidget *parent)
    : QWidget(parent)
    , image(800, 600, QImage::Format_ARGB32)
    , model("..\\data\\duck3.obj",
            "..\\data\\bird2_tex.jpg")
    , camZ(.8)
{
    view = viewport(image.width()/8, image.height()/8, image.width()*3/4, image.height()*3/4);

    render();
}

void Renderer::clearScreen()
{
    for (int i = 0; i < image.height(); ++i) {
        for (int j = 0; j < image.width(); ++j) {
            insertPixel(j, i, 0, 0, 0, 255, image);
        }
    }
}

void Renderer::render()
{
    clearScreen();

    float d = camZ;
    arma::mat proj = {
        {1,0,0,0},
        {0,1,0,0},
        {0,0,1,0},
        {0,0,-1/d,1}
    };

    std::vector<std::pair<std::vector<arma::ivec4>,int>> toDraw;

    for (int i = 0; i < model.nfaces(); ++i) {
        std::vector<arma::vec4> face = model.face(i);
        std::vector<arma::ivec4> screenCoords(3);

        arma::vec3 face_vec3[3];
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                face_vec3[i][j] = face[i][j];
            }
        }
        arma::vec3 n = arma::cross(face_vec3[1]-face_vec3[0], face_vec3[2]-face_vec3[0]);
        n = arma::normalise(n);

        arma::rowvec3 z; z[0] = face_vec3[0][0], z[1] = face_vec3[0][1], z[2] = face_vec3[0][2] - d;
        z = arma::normalise(z);

        auto dot = arma::as_scalar(z * n);

        if (dot > 0)
            continue;

        bool behindScreen = false;
        // Transform to screen coordinates
        for (int j = 0; j < (int)face.size(); ++j) {
            arma::vec4 p = face[j];
            p = view*proj*p;
            if ((behindScreen = p[3] <= d))
                break;
            p[0] = p[0] * (1/p[3]);
            p[1] = p[1] * (1/p[3]);
            for (int k = 0; k < 4; ++k)
                screenCoords[j][k] = p[k];
        }

        if (!behindScreen) {
            toDraw.push_back(std::make_pair(screenCoords,i));
        }
    }

    sort(toDraw.begin(), toDraw.end(), [](const std::pair<std::vector<arma::ivec4>,int> &l, const std::pair<std::vector<arma::ivec4>,int> &r) {
        int s1 = 0, s2 = 0;
        for (int j = 0; j < 3; ++j) {
            s1 += l.first[j][2];
            s2 += r.first[j][2];
        }
        return s1 > s2;
    });

    for (int i = 0; i < (int)toDraw.size(); ++i) {
        std::vector<arma::ivec4> t = toDraw[i].first;
        int j = toDraw[i].second;
        drawTriangle(t[0], t[1], t[2], j);
    }

    update();
}

void Renderer::drawTriangle(arma::ivec4 t0, arma::ivec4 t1, arma::ivec4 t2, int fi)
{
    if (t0[1]==t1[1] && t0[1]==t2[1]) return;

    if (!shouldDraw(t0, t1, t2)) return;

    std::vector<arma::vec2> uvs = model.getUVs(fi);

    if (t0[1]>t1[1]) {std::swap(t0, t1); std::swap(uvs[0], uvs[1]);}
    if (t0[1]>t2[1]) {std::swap(t0, t2); std::swap(uvs[0], uvs[2]);}
    if (t1[1]>t2[1]) {std::swap(t1, t2); std::swap(uvs[1], uvs[2]);}

    int total_height = t2[1]-t0[1];
    for (int i = 0; i < total_height; ++i) {
        bool secondHalf = i>t1[1]-t0[1] || t1[1]==t0[1];
        int segmentHeight = secondHalf ? t2[1]-t1[1] : t1[1]-t0[1];
        float alpha = (float)i/total_height;
        float beta = (float)(i-(secondHalf ? t1[1]-t0[1] : 0))/segmentHeight;

        int A = t0[0] + (t2[0] - t0[0]) * alpha;
        int B = secondHalf ? t1[0] + (t2[0]-t1[0])*beta : t0[0] + (t1[0]-t0[0])*beta;

        arma::vec2 uvA = uvs[0] + (uvs[2] - uvs[0]) * alpha;
        arma::vec2 uvB = secondHalf ? uvs[1] + (uvs[2] - uvs[1]) * beta : uvs[0] + (uvs[1]-uvs[0])*beta;

        if (A > B) {
            std::swap(A, B);
            std::swap(uvA, uvB);
        }

        for (int j = A; j <= B; ++j) {
            arma::ivec4 p = {j, t0[1]+i, 0, 0};
            float u,v,w;
            getBarycentric(p, t0, t1, t2, u, v, w);
            float U = uvs[0][0] * u + uvs[1][0] * v + uvs[2][0] * w;
            float V = uvs[0][1] * u + uvs[1][1] * v + uvs[2][1] * w;

            QColor c = model.getTextureColor(U, V);
            insertPixel(p[0], p[1], c.red(), c.green(), c.blue(), 255, image);
        }
    }
}

bool Renderer::shouldDraw(arma::ivec4 t0, arma::ivec4 t1, arma::ivec4 t2)
{
    arma::vec3 A, B, C;
    A[0] = t0[0], A[1] = t0[1], A[2] = t0[2];
    B[0] = t1[0], B[1] = t1[1], B[2] = t1[2];
    C[0] = t2[0], C[1] = t2[1], C[2] = t2[2];

    arma::vec3 n = arma::cross(B-A, C-A);

    arma::rowvec3 z; z[0] = 0, z[1] = 0, z[2] = camZ;

    auto dot = arma::as_scalar(z * n);

    return dot >= 0;
}

void Renderer::insertPixel(int x, int y, int r, int g, int b, int a, QImage &img)
{
    if (x < 0 || y < 0 || x >= img.width() || y >= img.height())
        return;

    uchar *line = img.scanLine(y);
    line[x*4]   = b; // Blue
    line[x*4+1] = g; // Green
    line[x*4+2] = r; // Red
    line[x*4+3] = a; // Alpha
}

void Renderer::getBarycentric(arma::ivec4 &p, arma::ivec4 &a, arma::ivec4 &b, arma::ivec4 &c, float &u, float &v, float &w)
{
    arma::ivec4 v0 = b - a;
    arma::ivec4 v1 = c - a;
    arma::ivec4 v2 = p - a;

    float den = v0[0] * v1[1] - v1[0] * v0[1];
    v = (v2[0] * v1[1] - v1[0] * v2[1]) / den;
    w = (v0[0] * v2[1] - v2[0] * v0[1]) / den;
    u = 1.0f - v - w;
}

arma::mat Renderer::viewport(int x, int y, int w, int h)
{
    int depth = 255;
    arma::mat m = {
        {w/2.f,0,0,x+w/2.f},
        {0,h/2.f,0,y+h/2.f},
        {0,0,depth/2.f,depth/2.f},
        {0,0,0,1}
    };

    return m;
}

void Renderer::paintEvent(QPaintEvent *)
{
    QPainter p(this);

    QTransform t;
    t = t.scale(1, -1);
    image = image.transformed(t);

    p.fillRect(0, 0, width(), height(), Qt::black);
    p.drawImage(0, 0, image);
}

void Renderer::moveX(int x)
{
    arma::vec4 t = model.position;
    t[0] = x/10.;
    model.translate(t);
    render();
}

void Renderer::moveY(int y)
{
    arma::vec4 t = model.position;
    t[1] = y/10.;
    model.translate(t);
    render();
}

void Renderer::moveZ(int z)
{
    arma::vec4 t = model.position;
    t[2] = z/10.;
    model.translate(t);
    render();
}

void Renderer::rotX(int alpha)
{
    float dt = alpha * M_PI/180;
    model.rotateX(dt);
    render();
}

void Renderer::rotY(int alpha)
{
    float dt = alpha * M_PI/180;
    model.rotateY(dt);
    render();
}

void Renderer::rotZ(int alpha)
{
    float dt = alpha * M_PI/180;
    model.rotateZ(dt);
    render();
}

void Renderer::scaleX(int x)
{
    arma::vec3 s = model.scaleVec;
    s[0] = x;///10.;
    model.scale(s);
    render();
}

void Renderer::scaleY(int y)
{
    arma::vec3 s = model.scaleVec;
    s[1] = y;///10.;
    model.scale(s);
    render();
}

void Renderer::scaleZ(int z)
{
    arma::vec3 s = model.scaleVec;
    s[2] = z;///10.;
    model.scale(s);
    render();
}
