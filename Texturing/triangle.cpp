#include "triangle.h"

Triangle::Triangle()
{

}

void Triangle::getBarycentric(Point p, float &u, float &v, float &w)
{
    Point *a = &points[0], *b = &points[1], *c = &points[2];
    Point v0 = Point(b->x - a->x, b->y - a->y);
    Point v1 = Point(c->x - a->x, c->y - a->y);
    Point v2 = Point(p.x - a->x, p.y - a->y);

    float den = v0.x * v1.y - v1.x * v0.y;
    v = (v2.x * v1.y - v1.x * v2.y) / den;
    w = (v0.x * v2.y - v2.x * v0.y) / den;
    u = 1.0f - v - w;
}
