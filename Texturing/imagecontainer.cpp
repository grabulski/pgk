#include "imagecontainer.h"

#include <QMouseEvent>
#include <QPainter>

ImageContainer::ImageContainer(QWidget *parent)
    : QWidget(parent)
{

}

void ImageContainer::insertPixel(int x, int y, int r, int g, int b, int a, QImage &img)
{
    if (x < 0 || y < 0 || x >= img.width() || y >= img.height())
        return;

    uchar *line = img.scanLine(y);
    line[x*4]   = b; // Blue
    line[x*4+1] = g; // Green
    line[x*4+2] = r; // Red
    line[x*4+3] = a; // Alpha
}

QColor ImageContainer::getPixel(int x, int y)
{
    return image.pixelColor(x, y);
}

void ImageContainer::drawLine(int x1, int y1, int x2, int y2, int r, int g, int b)
{
    if (x1 == x2 && y1 == y2) {
        insertPixel(x1, y1, r, g, b, 255, overlay);
        return;
    }

    double a = 0.0;

    if (x2 - x1 == 0)
        a = double(INT_MAX);
    else
        a = (y2 - y1) / double(x2 - x1);

    bool steep = std::abs(a) > 1.;

    if (steep) {
        // Swap top with bottom
        std::swap(x1, y1);
        std::swap(x2, y2);
        a = (y2 - y1) / double(x2 - x1);
    }

    if (x1 > x2) {
        // Swap left with right
        std::swap(x1, x2);
        std::swap(y1, y2);
    }

    double y = y1;
    if (steep) // Draw from y1 to y2
        for (int x = x1; x <= x2; ++x) {
            insertPixel(int(floor(y+0.5)), x, r, g, b, 255, overlay);
            y += a;
        }
    else // Draw from x1 to x2
        for (int x = x1; x <= x2; ++x) {
            insertPixel(x, int(floor(y+0.5)), r, g, b, 255, overlay);
            y += a;
        }
}

void ImageContainer::clearImage(QImage &img, int r, int g, int b, int a)
{
    for (int y = 0; y < img.height(); ++y) {
        uchar *line = img.scanLine(y);
        for (int x = 0; x < img.width(); ++x) {
            line[x*4] = b; // Blue
            line[x*4+1] = g; // Green
            line[x*4+2] = r; // Red
            line[x*4+3] = a; // Alpha
        }
    }
}

void ImageContainer::drawTriangle()
{
    Point p1, p2;
    for (int i = 0; i < (int)tri.points.size(); ++i) {
        p1 = tri.points[i%3];
        p2 = tri.points[(i+1)%3];
        drawLine(p1.x, p1.y, p2.x, p2.y, 255, 0, 0);
        qDebug("x1: %d, y1: %d, x2: %d, y2: %d", p1.x, p1.y, p2.x, p2.y);
    }
}

void ImageContainer::drawTexture()
{
    Point t0 = tri.points[0];
    Point t1 = tri.points[1];
    Point t2 = tri.points[2];

    if (t0.y==t1.y && t0.y==t2.y) return;

    std::vector<FPoint> oUVs = getUVs();

    if (t0.y>t1.y) std::swap(t0, t1);
    if (t0.y>t2.y) std::swap(t0, t2);
    if (t1.y>t2.y) std::swap(t1, t2);

    int totalHeight = t2.y-t0.y;
    for (int i = 0; i < totalHeight; ++i) {
        bool secondHalf = i>t1.y-t0.y || t1.y==t0.y;
        int segmentHeight = secondHalf ? t2.y-t1.y : t1.y-t0.y;
        float alpha = (float)i/totalHeight;
        float beta  = (float)(i-(secondHalf ? t1.y-t0.y : 0))/segmentHeight;

        int AX = t0.x + (t2.x-t0.x) * alpha;

        int BX = secondHalf ? t1.x + (t2.x-t1.x)*beta : t0.x + (t1.x-t0.x)*beta;

        if (AX > BX) {
            std::swap(AX, BX);
        }

        for (int j = AX; j <= BX; ++j) {
            float u,v,w;
            tri.getBarycentric(Point(j, t0.y+i), u, v, w);
            float U = oUVs[0].x * u + oUVs[1].x * v + oUVs[2].x * w;
            float V = oUVs[0].y * u + oUVs[1].y * v + oUVs[2].y * w;
            QColor c = other->image.pixelColor(other->image.width()*U, other->image.height()*V);
            insertPixel(j, t0.y+i, c.red(), c.green(), c.blue(), 255, overlay);
        }
    }
}

std::vector<FPoint> ImageContainer::getUVs()
{
    float oWidth = other->image.width();
    float oHeight = other->image.height();
    std::vector<FPoint> oUVs;
    for (Point p : other->tri.points) {
        float fx = p.x / oWidth;
        float fy = p.y / oHeight;
        oUVs.push_back(FPoint(fx, fy));
    }
    return oUVs;
}

void ImageContainer::reset(bool _locked)
{
    locked = _locked;
    clearImage(overlay, 0 , 0, 0, 0);
    tri.points.clear();

    update();
}

void ImageContainer::paintEvent(QPaintEvent *)
{
    QPainter p(this);

    p.fillRect(0, 0, width(), height(), Qt::black);
    p.drawImage(0, 0, image);
    p.drawImage(0, 0, overlay);
}

void ImageContainer::mouseReleaseEvent(QMouseEvent *event)
{
    int x = event->x();
    int y = event->y();

    if (locked || tri.points.size() == 3)
        return;

    tri.points.push_back(Point(x, y));

    if (tri.points.size() == 3) {
        locked = true;
        if (containerType == ContainerType::Source) {
            other->locked = false;
        } else if (containerType == ContainerType::Target) {
            drawTexture();
        }
        drawTriangle();
        update();
    }
}
