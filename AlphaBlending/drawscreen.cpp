#include "drawscreen.h"
#include "color.h"

#include <QPainter>

DrawScreen::DrawScreen(QWidget *parent)
    : QWidget(parent)
    //, background(imageWidth, imageHeight, QImage::Format_RGB32)
    , background("sciezka do ChinesePhotographer-2.jpg")
    , foreground("sciezka do SanderGrefte_scaled.jpg")
    , foreground2("sciezka do PeterAdamHoszang_scaled.jpg")
    , foreground3("sciezka do Landscape_with_Mountains_scaled.jpg")
    , blended(imageWidth, imageHeight, QImage::Format_RGB32)
{
    imagesToBlend[0] = {foreground, 0.f, "normal"};
    imagesToBlend[1] = {foreground2, 0.f, "normal"};
    imagesToBlend[2] = {foreground3, 0.f, "normal"};

}

void DrawScreen::blendImages(QImage &foreground, QImage &background, QImage &blended, float alpha, const QString &funcName)
{
    Color a, b, blend, fValue;

    for (int y = 0; y < foreground.height(); ++y) {
        uchar *fgLine = foreground.scanLine(y);
        uchar *bgLine = background.scanLine(y);
        uchar *blendedLine = blended.scanLine(y);
        for (int x = 0; x < foreground.width(); ++x) {
            b = Color(fgLine[x*4+2], fgLine[x*4+1], fgLine[x*4]);
            a = Color(bgLine[x*4+2], bgLine[x*4+1], bgLine[x*4]);
            fValue = blendModes.blendFunctions[funcName](a, b);
            blend.r = fValue.r*alpha + (1.0f-alpha)*a.r;
            blend.g = fValue.g*alpha + (1.0f-alpha)*a.g;
            blend.b = fValue.b*alpha + (1.0f-alpha)*a.b;
            blendedLine[x*4+2] = blend.r;
            blendedLine[x*4+1] = blend.g;
            blendedLine[x*4] = blend.b;
        }
    }
}

void DrawScreen::blendAllImages()
{
    blended = background;
    for (int i = 0; i < 3; ++i) {
        blendImages(imagesToBlend[i].image, blended, blended, imagesToBlend[i].alpha, imagesToBlend[i].funcName);
    }
}

void DrawScreen::blendModeChanged(const QString &mode)
{
    QObject *sender = QObject::sender();
    QString listName = sender->objectName();
    int index = listName[listName.length()-1].digitValue() - 1;
    imagesToBlend[index].funcName = mode;

    update();
}

void DrawScreen::alphaChanged(int a)
{
    QObject *sender = QObject::sender();
    QString listName = sender->objectName();
    int index = listName[listName.length()-1].digitValue() - 1;
    imagesToBlend[index].alpha = a/100.0f;

    update();
}

void DrawScreen::paintEvent(QPaintEvent *)
{
    QPainter p(this);

    p.fillRect(0, 0, width(), height(), Qt::blue);

    blendAllImages();

    p.drawImage(0, 0, blended);
}

