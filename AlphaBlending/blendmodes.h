#ifndef BLENDMODES_H
#define BLENDMODES_H

#include <QString>
#include <cmath>
#include <map>
#include "color.h"

class BlendModes
{
public:
    static Color normal(Color a, Color b)
    {
        return b;
    }

    static Color multiply(Color a, Color b)
    {
        return Color((a.r * b.r) >> 8, (a.g * b.g) >> 8, (a.b * b.b) >> 8);
    }

    static Color screen(Color a, Color b)
    {
        int cr = 255 - (((255 - a.r) * (255 - b.r)) >> 8);
        int cg = 255 - (((255 - a.g) * (255 - b.g)) >> 8);
        int cb = 255 - (((255 - a.b) * (255 - b.b)) >> 8);
        return Color(cr, cg, cb);
    }

    static Color additive(Color a, Color b)
    {
        int cr = std::min(a.r + b.r, 255);
        int cg = std::min(a.g + b.g, 255);
        int cb = std::min(a.b + b.b, 255);
        return Color(cr, cg, cb);
    }

    static Color difference(Color a, Color b)
    {
        return Color(std::abs(a.r - b.r), std::abs(a.g - b.g), std::abs(a.b - b.b));
    }

    typedef Color (*blendFunc)(Color, Color) ;

    std::map<QString, blendFunc> blendFunctions;

    BlendModes();
};

#endif // BLENDMODES_H
