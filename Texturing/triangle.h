#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "fpoint.h"
#include "point.h"
#include <vector>

class Triangle
{
public:
    std::vector<Point> points;

    Triangle();

    void getBarycentric(Point p, float &u, float &v, float &w);
};

#endif // TRIANGLE_H
