#ifndef DRAWSCREEN_H
#define DRAWSCREEN_H

#include "visualisation.h"
#include <QMap>
#include <QWidget>

class DrawScreen : public QWidget
{
    Q_OBJECT
private:
    int imageWidth = 256;
    int imageHeight = 256;
    QImage image;

    QMap<QString, Visualisation> visualisations;

    Visualisation *currentVis = nullptr;

    void insertPixel(int x, int y, int r, int g, int b);
    void clearScreen(int r, int g, int b);

    int red = 0;
    int green = 0;
    int blue = 0;
    int hue = 0;
    int saturation = 0;
    int value = 0;

public:
    explicit DrawScreen(QWidget *parent = nullptr);

protected:
    void paintEvent(QPaintEvent *);

public slots:
    void setRedValue(int val) {
        red = val;
        this->update();
    }

    void setGreen(int val) {
        green = val;
        this->update();
    }

    void setBlue(int val) {
        blue = val;
        this->update();
    }

    void setHue(int val) {
        hue = val;
        this->update();
    }

    void setSaturation(int val) {
        saturation = val;
        this->update();
    }

    void setValue(int val) {
        value = val;
        this->update();
    }

    void setCurrentVisualisation();

};

#endif // DRAWSCREEN_H
