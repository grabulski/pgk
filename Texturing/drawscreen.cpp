#include "drawscreen.h"

#include <QFileDialog>

DrawScreen::DrawScreen(QWidget *parent)
    : QWidget(parent)
    , layout(this)
{
    source.image = QImage(500, 500, QImage::Format_ARGB32);
    source.overlay = QImage(source.image.width(), source.image.height(), QImage::Format_ARGB32);
    source.clearImage(source.overlay, 0, 0, 0, 0);
    source.other = &target;
    source.resize(source.image.width(), source.image.height());

    target.image = QImage(source.image.width(), source.image.height(), QImage::Format_ARGB32);
    target.overlay = QImage(source.image.width(), source.image.height(), QImage::Format_ARGB32);
    target.clearImage(target.overlay, 0, 0, 0, 0);
    target.other = &source;
    target.containerType = ImageContainer::ContainerType::Target;
    target.locked = true;
    target.resize(target.image.width(), target.image.height());

    layout.addWidget(&source);
    layout.addWidget(&target);
}

void DrawScreen::resetAndResize(QString newImageName)
{
    source.reset(false);
    target.reset(true);

    source.image = QImage(newImageName);
    source.overlay = QImage(source.image.width(), source.image.height(), QImage::Format_ARGB32);

    target.image = QImage(source.image.width(), source.image.height(), QImage::Format_ARGB32);
    target.overlay = QImage(source.image.width(), source.image.height(), QImage::Format_ARGB32);
}

void DrawScreen::reset()
{
    source.reset(false);
    target.reset(true);
}

void DrawScreen::open()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::ExistingFile);
    dialog.setNameFilter(tr("Images (*.jpg)"));

    if (dialog.exec()) {
        QStringList files = dialog.selectedFiles();
        resetAndResize(files.at(0));
    }
}
