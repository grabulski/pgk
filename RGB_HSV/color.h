#ifndef COLOR_H
#define COLOR_H


class Color
{
public:
    int r_h;
    int g_s;
    int b_v;

    Color();
    Color(int _r_h, int _g_s, int _b_v);

    Color toRGB();
};

#endif // COLOR_H
